# Drupal in Docker good defaults

##  📝 Table of Contents

- [Getting started](#getting_started)

## 🏁 Getting started <a name="getting_started"></a>

The instructions below will allow you to get a cooy of this project up and running locally for development purpose.

### Prerequisites <a name="getting_started_prerequisites"></a>

Your host running OS has to be Linux or MacOS (_Not tested on windows yet_).

#### Host needed software

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [docker ^19.03](https://docs.docker.com/install/)
- [docker-compose ^1.25.4](https://docs.docker.com/compose/install/)
- [composer ^1.9.3](https://getcomposer.org/doc/00-intro.md) (optional)

#### Host preparation

1. Create or ensure a `~/.composer` folder exists if you are going to use the [docker composer] image to download the drupal code on your local host.
2. Clone this repository locally: `git clone git@gitlab.com:nikathone/drupal-docker-good-defaults.git drupal_docker && cd drupal_docker`
3. Download drupal 8:
  - Using composer installed on the local host run: `composer --no-interaction create-project drupal/recommended-project:^8.8 code`
  - Using docker composer image: `docker container run -it --rm -v $HOME/.composer:/tmp -v $PWD:/app composer:1.9.3 --no-interaction --ignore-platform-reqs create-project drupal/recommended-project:^8.8 code`
  - Using composer add drush: `cd code/ && composer require drush/drush`
  - You can also clone your existing drupal code, which has to be composer managed, under `./code`: `git clone git@<remote> code`
4. Create local `.dockerignore` file: `cp local.dockerignore` `.dockerignore`
5. Create `.env` file to be used by `docker-compose`: `cp docker-compose.env .env`
6. Create Drupal `.env` file: `cp drupal.env code/.env`
7. Create a folder for the database service volume: `mkdir -p database_files`
8. Create a folder for drupal public and private files: `mkdir -p drupal_files/public drupal_files/private` 

### Running drupal

```
docker-compose up -d --build
```
You can add a `-p <project_name>` on the command above so that docker use the project name instead of auto creating one by default.

## 🧱 Services

### Database

[Mariadb](https://hub.docker.com/_/mariadb) version 10.3.22 is used for the database service. 

This service creates a named volume which use the local driver to bind mount `./database_files` into the container to persist the database files locally.

### Webserver/Drupal

This repo integrate with [gitlab ci](./.gitlab-ci.yml) to build a base image containing PHP 7.2.27 and NGINX 1.17.8

The base image is extended by another [drupal dockerfile](./drupal.Dockerfile) to add any related drupal customizations. This image uses docker multi stage to separate build, production and development requirements setup.

The drupal service also need `drupal_files` on the host to persist drupal files. There are various un-named volumes which are also created to hold various composer generated dependencies.

