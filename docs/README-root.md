# DRUPAL OPS

Implement various operations tools which are being used by drupal sites. The
tools served for base image build for php and database, various gitlab ci jobs.

The various tools used are

- Ansible: as a self-documenting scripting tool for deployment and spin of local site
- Docker: for containerization
- Docker compose: to easy the running and setup of multiple container and their related services and network on local
- Docker swarm: for container orchestration.
- Gitlab: for automatic deployment and docker image registry

## 📝 Table of Contents

- [Getting Started](#getting_started)
- [Components](#components)
- [Deployment](#deployment)

## 🏁 Getting Started <a name="getting_started"></a>

The instructions below will allow you to get a copy of this project up and
 running locally for development and testing purposes. See [deployment](#deployment)
 for notes about app deployment.

### Prerequisites <a name="getting_started_prerequisites"></a>

Your running OS has to Linux or MacOS. (_Not tested on windows yet_)

#### Needed software

- git
- docker ^19.03
- ansible ^2.8.6

#### OPS Local install

1. Create or use a local folder where the local apps will be downloaded (`mkdir -p ~/apps`).
 e.g. `~/apps/`
2. Create a `~/.composer` to be used as composer backup storage between your
 local machine and docker containers
3. Clone (`cd ~/apps && git clone <projecct>`)
 this project so that it end up at `<local_folder>/ops`. e.g. `~/apps/ops`
4. copy `~/apps/ops/local.dockerignore` to `~/apps/.dockerignore`.
5. Log into the private registry: `docker login <registry-url> --username <username>`
  where `<username>` is your UoG central username and on password prompt enter
  your UoG central password.

### Usage with project <a name="getting_started_usage"></a>

The app/site machine name should start with `site_` to make it easier to ignore
 multiple folders of sites in the local dockerignore using `/site_*`. Also the
 current structure assume that any drupal code is managed with composer and the
 doc root is relocated under the `web` folder. Check https://github.com/drupal-composer/drupal-project
 to see how the folder structure of the drupal code should be. Ideally we should
 start to use http://github.com/drupal/recommended-project, which was introduced
 in drupal 8.8.x release.[](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#s-using-drupalrecommended-project)

#### New drupal site/app <a name="getting_started_usage_new_app"></a>

Follow the steps below if you want to work on a new site which is not deployed
 yet and it doesn't have any remote repository. _(The start project playbook
 needs to be updated and start to use the drupal/recommended-project)_

1. Edit the [sites list](/ansible/vars/sites.yml) ansible
 variable dict to add site/app details
2. Commit and push new supported site to ops
3. Navigate back at `~/apps` folder
4. Run `ansible-playbook -i ops/ansible/inventory/hosts.yml -l localhost -e new_project_name=<machine_name> ops/ansible/start_project.yml`.
 Make sure to replace `machine_name` with the new site/app machine name added in #1
5. Once the command in #4 finish to run you should have a new folder at `~/apps/<machine_name>`
6. Start the local site using docker with `docker-compose -p <machine_name> -f ops/docker/docker-compose.yml up --build`
7. See [Deployment](#deployment) for more information about deploying the site on
<host-server>

#### Existing local drupal site/app

If you have an existing drupal local site you have been testing or deployed
 somewhere else and you would like to test and deploy it using this architecture
 you should follow *all the steps to install local ops* above then:

1. `cd ~/apps/` or the folder where the local ops folder is under
2. Edit the [sites list](/ansible/vars/sites.yml) ansible
 variable dict to add the local site/app details
3. Commit and push new supported local site to ops
4. Navigate back at `~/apps` folder
5. Create a `site_<local_site_app_machine_name>` folder
6. Move or make a copy of the local site under `site_<local_site_app_machine_name>`,
 by making sure that the site root folder is named `code` so that you have
 `~/apps/site_<local_site_app_machine_name>/code`
7. Navigate under `~/apps/site_<local_site_app_machine_name>/` and create
 `files/public` and `files/private` for local storage files folder bind mount
 and a `mysql` folder for the database
8. Edit `code/.gitignore` and add `!/web/sites/default/settings.php` so that
 `settings.php` can be committed
9. Edit `code/web/sites/default/settings.php` and add the snippet below

```php
/**
 * Load project override configuration, if available..
 */
if (file_exists($app_root . '/' . $site_path . '/settings.project.php')) {
  include $app_root . '/' . $site_path . '/settings.project.php';
}
```

right before the inclusion of `settings.local.php`. Make sure that
`code/web/sites/default/settings.php` doesn't contain any db credentials.

10. Copy [`~/apps/ops/ansible/templates/settings.project.php.j2`](ansible/templates/settings.project.php.j2)
 and paste it as `code/web/sites/default/settings.project.php`
11. Copy [`~/apps/ops/ansible/templates/drupal.gitlab-ci.yml.j2`](ansible/templates/drupal.gitlab-ci.yml.j2)
 and paste it as `code/.gitlab-ci.yml.j2`
12. Move back under `~/apps/` and edit `~/apps/.dockerignore` then add `!/site_<local_site_app_machine_name>`
 at the end of the file to prevent the current site from being ignored by docker
13. Start the local site using docker with `docker-compose -p site_<local_site_app_machine_name> -f ops/docker/docker-compose.yml up --build`
14. See [Deployment](#deployment) for more information about deploying the site on
<host-server>

## 🧱 Components <a name="components"></a>

## Docker

The drupal websites and their related services such as the database are being be
 deployed inside docker containers. This repository contains the base images for:

### Mariadb

The mariadb image dockerfile is located at [`docker/database/Dockerfile`](/docker/database/Dockerfile)
 and it's using the [official mariadb image](https://hub.docker.com/_/mariadb)
 as it's starting point.

This image is mostly used to fine tune various mariadb settings and to set
 `mysql` user as the default user when the container is running instead of
  root.

A new image is built and push to the registry every time there is a change in
 any file in `docker/database/*` folder.

#### Maintenance

Changing/Updating the version of mariadb is done by editing the version
 directly in [`docker/database/Dockerfile`](/docker/database/Dockerfile) and
 `MARIADB_VERSION` environment variable in [.gitlab-ci.yml](.gitlab-ci.yml).
 After that, a git commit and push should follow then the following two things
 need to happen in order for the updated database image to be used during:

- deployment, an [ansible variable `docker_db_version`](ansible/vars/main.yml#14)
 needs to be updated right after the image build is done and pushed into the
 registry.
- development, the [mariadb service](docker/docker-compose.yml) image version
 also needs to be updated right after the image build is done and pushed into the
 registry.

### Webserver (NGINX/PHP)

There is only one image to run both NGINX and PHP(-FPM), while this doesn't
 fall inline with Docker's vision of one container per responsibility/process,
 it requires more work to have to maintain separate containers for both NGINX
 and PHP since they both need access to the code base. We use [supervisord](http://supervisord.org/)
  to control both system.

This ops repository is responsible to build the base webserver image using the
 [`docker/webserver/base.Dockerfile`](/docker/webserver/base.Dockerfile). Then
 each app/sites use [`docker/webserver/Dockerfile`](/docker/webserver/Dockerfile)
  to build the app/site image to be deployed.

A new base image is built every time `docker/webserver/base.Dockerfile` and/or
 `docker/webserver/supervisord.conf` files are updated.

#### Maintenance/Update

Since we can't trigger an image build by only updating related php and nginx
 environment variables in [.gitlab-ci.yml](.gitlab-ci.yml), we then need to
 update the version in  [`docker/webserver/base.Dockerfile`](/docker/webserver/base.Dockerfile)
 as well.

## 🚀 Deployment <a name="deployment"></a>

Pushing a new site to gitlab should take care of building it's docker image and
 deploying it using the gitlab-ci files in [gitlab-ci/](/gitlab-ci) folder.

Given that you have followed all the needed steps under
 [usage](#getting_started_usage) you should be able to

1. Go to https://gitlab.com/ then create a new project under
 the drupal group.
2. Add needed ops deployment and admin access token to the newly created
 project by navigating at `https://gitlab.com/<project_machine_name>/-/settings/ci_cd` and a.
 The needed keys are `DRUPAL_OPS_DEPLOY_TOKEN` and `ADMIN_ACCESS_TOKEN` and
 their respective settings can be found at [ops ci/cd settings variables](https://gitlab.com/drupal/ops/-/settings/ci_cd#js-cicd-variables-settings)
3. Go back to the create new project page and follow the instructions to push an
 existing local project on gitlab
