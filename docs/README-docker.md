# OPS Docker

This repository can build up to 4 images for each commits. Every sub-folder at
 this level contains the necessary for the images building.

## [Ansible](ansible/)

Using [geerlingguy/docker-centos7-ansible](https://hub.docker.com/r/geerlingguy/docker-centos7-ansible/)
 as the base image it adds ssh libs and rsync packages. Then the produced image
 contains the [ansible](../ansible/) folder as ops and it's being used as in one
 of the gitlab runner responsible for deployment.

For authentication purpose, the gitlab runner bind-mount the deployer user ssh
 keys.

## [dind](dind/)

The docker in docker(dind) custom in image built on top of the
 [official docker](https://hub.docker.com/_/docker/) image, adds curl package
 which is needed in our various workflow. This image is also being used in one
 of our gitlab runner responsible of building and pushing docker custom images.

## [database](database/)

Using the [official mariadb](https://hub.docker.com/_/mariadb/) image as it's
 starting point, this custom image provides custom setting and fine tuning of
 various settings. These customization are performed using
 [confd](https://github.com/kelseyhightower/confd) and various environment
  variables.

Updating to the latest mariadb consist of changing the [`MARIADB_VERSION`
 variable](../.gitlab-ci.yml) and also changing the default docker
 `mariadb_version` argument in the [database Dockerfile](database/Dockerfile) to
 to trigger the build.

### [Environment/System variables](https://mariadb.com/kb/en/library/server-system-variables/)

Most of the variables are linked to their documentation page and there is more
 information about the custom configurations in the mysql
 [cnf template file](database/templates/confd/templates/config-file.cnf.tmpl).

| Variable                                 | Default Value    |
| ---------------------------------------- | ---------------- |
| `MYSQL_PORT`                             | `3306`           |
| `MYSQL_CLIENT_DEFAULT_CHARACTER_SET`     | `utf8`           |
| [`MYSQL_PERFORMANCE_SCHEMA`]             | `OFF`            |
| [`MYSQL_MAX_CONNECTIONS`]                | `151`            |
| [`MYSQL_WAIT_TIMEOUT`]                   | `600`            |
| [`MYSQL_MAX_ALLOWED_PACKET`]             | `128M`           |
| [`MYSQL_THREAD_CACHE_SIZE`]              | `128`            |
| [`MYSQL_SORT_BUFFER_SIZE`]               | `4M`             |
| [`MYSQL_TMP_TABLE_SIZE`]                 | `32M`            |
| [`MYSQL_MAX_HEAP_TABLE_SIZE`]            | `32M`            |
| [`MYSQL_JOIN_BUFFER_SIZE`]               | `262144`         |
| [`MYSQL_KEY_BUFFER_SIZE`]                | `256M`           |
| [`MYSQL_TABLE_OPEN_CACHE`]               | `512`            |
| [`MYSQL_MYISAM_SORT_BUFFER_SIZE`]        | `256M`           |
| [`MYSQL_READ_BUFFER_SIZE`]               | `2M`             |
| [`MYSQL_READ_RND_BUFFER_SIZE`]           | `4M`             |
| [`MYSQL_QUERY_CACHE_LIMIT`]              | `1M`             |
| [`MYSQL_QUERY_CACHE_TYPE`]               | `DEMAND`         |
| [`MYSQL_LOG_WARNINGS`]                   | `2`              |
| [`MYSQL_SLOW_QUERY_LOG`]                 | `1`              |
| [`MYSQL_LONG_QUERY_TIME`]                | `2`              |
| [`MYSQL_EXPIRE_LOGS_DAYS`]               | `10`             |
| [`MYSQL_MAX_BINLOG_SIZE`]                | `100M`           |
| [`MYSQL_DEFAULT_STORAGE_ENGINE`]         | `InnoDB`         |
| [`MYSQL_INNODB_LOG_FILE_SIZE`]           | `64M`            |
| [`MYSQL_INNODB_BUFFER_POOL_SIZE`]        | `256M`           |
| [`MYSQL_INNODB_LOG_BUFFER_SIZE`]         | `8M`             |
| [`MYSQL_INNODB_FILE_PER_TABLE`]          | `1`              |
| [`MYSQL_INNODB_OPEN_FILES`]              | `512`            |
| [`MYSQL_INNODB_IO_CAPACITY`]             | `400`            |
| [`MYSQL_INNODB_FLUSH_METHOD`]            | `O_REDIRECT`     |
| [`MYSQL_INNODB_LOCK_WAIT_TIMEOUT`]       | `50`             |
| [`MYSQL_BINLOG_FORMAT`]                  | `ROW`            |
| [`MYSQL_INNODB_FLUSH_LOG_AT_TRX_COMMIT`] | `1`              |
| [`MYSQL_DUMP_MAX_ALLOWED_PACKET`]        | `256M`           |

[`MYSQL_PERFORMANCE_SCHEMA`]: https://mariadb.com/kb/en/library/performance-schema-system-variables#performance_schema
[`MYSQL_MAX_CONNECTIONS`]: https://mariadb.com/kb/en/library/server-system-variables#max_connections
[`MYSQL_WAIT_TIMEOUT`]: https://mariadb.com/kb/en/library/server-system-variables#wait_timeout
[`MYSQL_MAX_ALLOWED_PACKET`]: https://mariadb.com/kb/en/library/server-system-variables#max_allowed_packet
[`MYSQL_THREAD_CACHE_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables#thread_cache_size
[`MYSQL_SORT_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables#sort_buffer_size
[`MYSQL_TMP_TABLE_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables#tmp_table_size
[`MYSQL_MAX_HEAP_TABLE_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables#max_heap_table_size
[`MYSQL_JOIN_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables#join_buffer_size
[`MYSQL_KEY_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/myisam-system-variables/#key_buffer_size
[`MYSQL_TABLE_OPEN_CACHE`]: https://mariadb.com/kb/en/library/server-system-variables#table_open_cache
[`MYSQL_MYISAM_SORT_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/myisam-system-variables/#myisam_sort_buffer_size
[`MYSQL_READ_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables/#read_buffer_size
[`MYSQL_READ_RND_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/server-system-variables/#read_rnd_buffer_size
[`MYSQL_QUERY_CACHE_LIMIT`]: https://mariadb.com/kb/en/library/server-system-variables#query_cache_limit
[`MYSQL_QUERY_CACHE_TYPE`]: https://mariadb.com/kb/en/library/server-system-variables#query_cache_type
[`MYSQL_LOG_WARNINGS`]: https://mariadb.com/kb/en/library/server-system-variables/#log_warnings
[`MYSQL_SLOW_QUERY_LOG`]: https://mariadb.com/kb/en/library/server-system-variables#slow_query_log
[`MYSQL_LONG_QUERY_TIME`]: https://mariadb.com/kb/en/library/server-system-variables#long_query_time
[`MYSQL_EXPIRE_LOGS_DAYS`]: https://mariadb.com/kb/en/library/replication-and-binary-log-system-variables/#expire_logs_days
[`MYSQL_MAX_BINLOG_SIZE`]: https://mariadb.com/kb/en/library/replication-and-binary-log-system-variables/#max_binlog_size
[`MYSQL_DEFAULT_STORAGE_ENGINE`]: https://mariadb.com/kb/en/library/server-system-variables#default_storage_engine
[`MYSQL_INNODB_LOG_FILE_SIZE`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_log_file_size
[`MYSQL_INNODB_BUFFER_POOL_SIZE`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_buffer_pool_size
[`MYSQL_INNODB_LOG_BUFFER_SIZE`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_log_buffer_size
[`MYSQL_INNODB_FILE_PER_TABLE`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_file_per_table
[`MYSQL_INNODB_OPEN_FILES`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_open_files
[`MYSQL_INNODB_IO_CAPACITY`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_io_capacity
[`MYSQL_INNODB_FLUSH_METHOD`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_flush_method
[`MYSQL_INNODB_LOCK_WAIT_TIMEOUT`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_lock_wait_timeout
[`MYSQL_BINLOG_FORMAT`]: https://mariadb.com/kb/en/library/replication-and-binary-log-system-variables/#binlog_format
[`MYSQL_INNODB_FLUSH_LOG_AT_TRX_COMMIT`]: https://mariadb.com/kb/en/library/xtradbinnodb-server-system-variables#innodb_flush_log_at_trx_commit
[`MYSQL_DUMP_MAX_ALLOWED_PACKET`]: https://mariadb.com/kb/en/library/server-system-variables/#max_allowed_packet

## [webserver](webserver/)

The webserver folder contains two dockerfiles:

1. One [base dockerfile](webserver/base.Dockerfile), extending the
 [official php fpm](https://hub.docker.com/_/php) image, which contains most of
 the needed php extensions and nginx modules to run a drupal 8 website. Ideally
 this dockerfile should be used to add new php extensions and/or nginx modules.
To update both php and/or nginx the [main .gitlab-ci.yml](../.gitlab-ci.yml)
 file should be edited so that `PHP_VERSION`, `NGINX_VERSION` and
 `NGINX_NJS_VERSION` reflect the newer version and inside this dockerfile, the
 default arguments related those newer versions should also be updated to
 trigger the job which build and push the base image into the registry.
2. The [app/site dockerfile](webserver/Dockerfile), built on top of the base
  Dockerfile, is a multi-stage dockerfile which takes care of including a:

    I. A composer build stage: Uses the
     [official composer](https://hub.docker.com/_/composer) image to run
     `composer install` with optional arguments based on build environment.

    II. A UoG theme front end theme build stage: compile the final needed css
     files using the [official node](https://hub.docker.com/_/node) image.

    III. A base stage built on top of the base webserver docker image
     responsible:

     - to gather all the files produced by the two build stages above,
     - to create the site app/site runner user and group,
     - to add [custom scripts](webserver/bin) needed by the image during build
      and run time,
     - to add and process configuration templates using confd,
     - to instantiate the `ENTRYPOINT`, `CMD` and `EXPOSE` dockerfile
      directives,
     - to copy the rest of the app/site code into the image

    IV. A production stage extending the base stage above and so far it's just
     put the default `php.ini` production in place.

    V. A development stage, also extending the base image, adds and enables
     php development extensions such as xdebug then apply some development
     customizations.

### Environment variables

TODO

## [Docker compose](./docker-composer.yml)

Found at the root of this folder, it's being used for local development and it
 takes care of bringing both the database and webserver image into life on your
 local machine. See the main README on how to get start.

## [Docker swarm](../ansible/templates/stack.yml.j2)

It's the container orchestration tool being used on development and eventually
 on production. Since the compose file for a particular app/site needs a lot of
 dynamic values this file is generated during deployment, by ansible, based on
 the template found at
  [/ansible/templates/stack.yml.j2](../ansible/templates/stack.yml.j2).
