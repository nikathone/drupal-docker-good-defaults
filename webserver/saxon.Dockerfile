ARG base_image_tag

FROM registry.gitlab.com/nikathone/drupal-docker-good-defaults/php-nginx:${base_image_tag}

ENV LIBSAXON_VERSION=12.0 \
  LIBSAXON_EDITION=HE \
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/

ENV LIBSAXON_DOWNLOAD_FILE_NAME=libsaxon-${LIBSAXON_EDITION}C-linux-v${LIBSAXON_VERSION}

# 0. We need a higher GLIBC version, so install libc6
RUN set -ex; \
  echo "deb https://ftp.debian.org/debian bookworm main" >> /etc/apt/sources.list \
	&& apt-get update \
	&& apt-get -t bookworm install --no-install-recommends --no-install-suggests -y \
    libc6 libc6-dev libc6-dbg libcrypt1; \
    apt-get remove --autoremove --purge -y opencl-headers; \
    rm -r /var/lib/apt/lists/*

WORKDIR /tmp
RUN set -ex; \
  # 1. Downloading SaxonC software from saxonica.com.
  curl -L https://www.saxonica.com/download/$LIBSAXON_DOWNLOAD_FILE_NAME.zip --output $LIBSAXON_DOWNLOAD_FILE_NAME.zip; \
  # 2. Unpacking the downloaded package.
  unzip $LIBSAXON_DOWNLOAD_FILE_NAME.zip; \
  mv /tmp/$LIBSAXON_DOWNLOAD_FILE_NAME /tmp/saxon; \
  rm $LIBSAXON_DOWNLOAD_FILE_NAME.zip

# 3. Copying the core SaxonC library to the install location.
WORKDIR /tmp/saxon
RUN set -ex; \
  cp libs/nix/* /usr/lib/

# 4. Installing the php extension.
WORKDIR /tmp/saxon/Saxon.C.API/
RUN set -ex; \
  # @todo remove once https://saxonica.plan.io/issues/5856 have a fix release.
  sed -i "s|setRelocate|setRelocatable|g" php8_Xslt30Processor.cpp; \
  phpize; \
  ./configure --enable-saxon; \
  make -j$(nproc); \
  make install; \
  # Enabling the saxon php extension.
  docker-php-ext-enable saxon \
  # Cleaning up.
  && rm -rf /tmp/saxon

WORKDIR $APP_ROOT
