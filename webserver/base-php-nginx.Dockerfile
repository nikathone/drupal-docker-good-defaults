ARG php_version=8.2.27
ARG php_pkg_release=fpm-bullseye

FROM golang:1.22.3-bullseye as confd

RUN set -ex; \
    apt-get update && apt-get install --no-install-recommends --no-install-suggests -y \
    git \
    && mkdir -p $GOPATH/src/github.com/kelseyhightower \
    && git clone https://github.com/kelseyhightower/confd.git $GOPATH/src/github.com/kelseyhightower/confd \
    && cd $GOPATH/src/github.com/kelseyhightower/confd \
    && make \
    && cp bin/confd /usr/local/bin/confd

FROM php:${php_version}-${php_pkg_release}

LABEL dev.nikathone.name="php-nginx" \
      dev.nikathone.vcs-url="gitlab.com/nikathone/drupal-docker-good-defaults"

ARG nginx_version=1.22.1
ARG nginx_njs_version=0.7.7
ARG nginx_pkg_release=1~bullseye
ARG CONFD_VERSION=0.20.0
ARG CONFD_SHA256SUM="83bbcc1a87a5f81e66dcfc11e86c0e2110af8ff06ae3afc58d382462418ce5ed"
ARG WKHTMLTOPDF_VERSION=0.12.6
ARG IMAGE_MAGICK_VERSION=6.9.11-51

ENV FILES_DIR="/mnt/files" \
    APP_ROOT="/var/www/app" \
    APP_DOCROOT="/var/www/app/web"

# @TODO use specific versions of some of these to avoid version conflict or breaking
# Install apt dependencies
RUN set -ex; \
    apt-get update && apt-get install --no-install-recommends --no-install-suggests -y \
    # utilitities cmd
    dos2unix rsync wget findutils iputils-ping \
    # for imap
    libc-client2007e-dev libkrb5-dev \
    # for ssh client only
    openssh-client \
    # for git
    git \
    # for bz2
    bzip2 libbz2-dev \
    # for gd and wkhtmltopdf
    libfreetype6 libfreetype6-dev libpng-tools libjpeg62-turbo libjpeg62-turbo-dev libpng16-16 libgmp-dev libwebp-dev libpng-dev libwebp6 libgomp1 libwebpdemux2 ghostscript \
    # for wkhtmltopdf
    fontconfig libx11-6 libxcb1 libxext6 libxrender1 xfonts-75dpi xfonts-base \
    # For image optimization
    jpegoptim optipng pngquant \
    # For imagick
    libmagickwand-dev librsvg2-bin opencl-headers \
    # For nginx cgi-fcgi
    libfcgi0ldbl \
    # For memcached
    libmemcached-dev \
    # for intl
    libicu-dev \
    # for mcrypt
    libmcrypt-dev \
    # for ldap
    libldap2-dev \
    # for zip
    libzip-dev zip unzip \
    # for xslt
    libxslt1-dev \
    # for postgres
    libpq-dev \
    # for tidy
    libtidy-dev \
    # for yaml
    libyaml-dev \
    # for command like drush sqlc/sqlq which need a mysql client
    mariadb-client \
    # for supervsisor (http://supervisord.org/)
    supervisor; \
    # install imagemagick from source
    git clone https://github.com/ImageMagick/ImageMagick6.git /tmp/ImageMagick && \
    cd /tmp/ImageMagick && git checkout -q ${IMAGE_MAGICK_VERSION} && \
    ./configure --with-rsvg=yes && make && make install && \
    ldconfig /usr/local/lib && \
    rm -rf /tmp/ImageMagick && cd -; \
    # install confd
    # cd /tmp && wget "https://github.com/abtreece/confd/releases/download/v${CONFD_VERSION}/confd-v${CONFD_VERSION}-linux-amd64.tar.gz" \
    # && tar -xvf /tmp/confd-v${CONFD_VERSION}-linux-amd64.tar.gz && cd - \
    # && mv /tmp/confd /usr/local/bin/confd; \
    # chmod +x /usr/local/bin/confd; \
    # verify confd signature
    # sha256sum /tmp/confd-v${CONFD_VERSION}-linux-amd64.tar.gz | grep -q "${CONFD_SHA256SUM}" || exit 1; \
    # install wkhtmltopdf
    dpkgArch="$(dpkg --print-architecture)" \
    && case "$dpkgArch" in \
      amd64|i386) \
        wget -O "/tmp/wkhtmltox_${WKHTMLTOPDF_VERSION}.1-2.bullseye.deb" "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/${WKHTMLTOPDF_VERSION}.1-2/wkhtmltox_${WKHTMLTOPDF_VERSION}.1-2.bullseye_${dpkgArch}.deb"; \
      ;; \
      *) \
        echo "Unsupported architecture by wkhtmltopdf team" && exit 1; \
      ;; \
    esac \
    && apt-get install "/tmp/wkhtmltox_${WKHTMLTOPDF_VERSION}.1-2.bullseye.deb"; \
    ln -s /usr/local/bin/wkhtmltopdf /usr/bin/wkhtmltopdf; \
    apt-get remove --autoremove --purge -y opencl-headers; \
    rm -r /var/lib/apt/lists/* && rm "/tmp/wkhtmltox_${WKHTMLTOPDF_VERSION}.1-2.bullseye.deb"

COPY --from=confd /usr/local/bin/confd /usr/local/bin/confd

SHELL ["/bin/bash", "-c"]

# Install and enable php extensions using the helper script provided by the
# base image
RUN set -xe; \
    \
    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-configure zip \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-configure intl --enable-intl \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu \
    && docker-php-ext-install -j "$(nproc)" \
      gd \
      bcmath \
      bz2 \
      calendar \
      exif \
      gettext \
      gmp \
      imap \
      intl \
      ldap \
      mysqli \
      opcache \
      pcntl \
      pdo_mysql \
      pdo_pgsql \
      pgsql \
      soap \
      sockets \
      tidy \
      xsl \
      zip \
    # pecl related extensions
    && pecl install \
      apcu-5.1.22 \
      imagick-3.7.0 \
      memcached-3.2.0 \
      yaml-2.2.2 \
    && docker-php-ext-enable \
      apcu \
      imagick \
      memcached \
      yaml \
    && if [[ "${PHP_VERSION:0:3}" == "8.1" ]]; then \
      pecl install mcrypt-1.0.5; \
      docker-php-ext-enable mcrypt; \
    fi

# install nginx (adapted from official nginx Dockerfile
# https://github.com/nginxinc/docker-nginx/blob/fef51235521d1cdf8b05d8cb1378a526d2abf421/stable/debian/Dockerfile)
ENV NGINX_VERSION=${nginx_version} \
    NGINX_PKG_RELEASE=${nginx_pkg_release} \
    NJS_VERSION=${nginx_njs_version} \
    NGINX_USER=www-data \
    NGINX_USER_GROUP=www-data

RUN set -xe \
    # Ensure nginx user/group first, to be consistent throughout docker variants
    && user_exists=$(id -u ${NGINX_USER} > /dev/null 2>&1; echo $?) \
    && if [ user_exists -eq 0 ]; then \
      addgroup --system --gid 101 ${NGINX_USER_GROUP}; \
      adduser --system --disabled-login \
        --ingroup ${NGINX_USER_GROUP} --no-create-home \
        --home /nonexistent --gecos "${NGINX_USER} user" \
        --comment "nginx user" \
        --shell /bin/false --uid 101 ${NGINX_USER}; \
    fi \
    && apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y gnupg2 ca-certificates lsb-release \
    && \
    NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
    NGINX_GPGKEY_PATH=/etc/apt/keyrings/nginx-archive-keyring.gpg; \
    export GNUPGHOME="$(mktemp -d)"; \
    found=''; \
    for server in \
      hkp://keyserver.ubuntu.com:80 \
      pgp.mit.edu \
    ; do \
      echo "Fetching GPG key $NGINX_GPGKEY from $server"; \
      gpg --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$NGINX_GPGKEY" && found=yes && break; \
    done; \
    test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPGKEY" && exit 1; \
    mkdir -p /etc/apt/keyrings && gpg --export "$NGINX_GPGKEY" > "$NGINX_GPGKEY_PATH"; \
    rm -rf "$GNUPGHOME"; \
    apt-get remove --purge --auto-remove -y gnupg2 && rm -rf /var/lib/apt/lists/* \
    && nginxPackages=" \
      nginx=${NGINX_VERSION}-${NGINX_PKG_RELEASE} \
      nginx-module-xslt=${NGINX_VERSION}-${NGINX_PKG_RELEASE} \
      nginx-module-geoip=${NGINX_VERSION}-${NGINX_PKG_RELEASE} \
      nginx-module-image-filter=${NGINX_VERSION}-${NGINX_PKG_RELEASE} \
      nginx-module-njs=${NGINX_VERSION}+${NJS_VERSION}-${NGINX_PKG_RELEASE} \
    " \
    # Arches officialy built by upstream
    && echo "deb [signed-by=$NGINX_GPGKEY_PATH] https://nginx.org/packages/debian `lsb_release -cs` nginx" | tee /etc/apt/sources.list.d/nginx.list \
    && apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y \
      $nginxPackages \
      gettext-base \
    && apt-get remove --purge --auto-remove -y \
    && rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/nginx.list \
    # Create default place to store files generated by the app
    && install -o ${NGINX_USER} -g ${NGINX_USER} -d \
      "${APP_DOCROOT}" \
      "${FILES_DIR}/public" \
      "${FILES_DIR}/private" \
      /etc/nginx/conf.d \
      /var/cache/nginx \
      /var/lib/nginx; \
    mkdir -p "${FILES_DIR}" && chmod -R 775 "${FILES_DIR}"

EXPOSE 80 443

# Copy custom supervisord configuration file into the container.
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Copy customized configurations for php and nginx which can then be instantiated
# by any other image which extend this one.
COPY --chown=root:root config/templates/nginx_php_confd /confd_templates
COPY --chown=root:root config/templates/app_confd /drupal/confd

# Also copying script which can be used by a drupal image. Other non drupal app
# which are building from this image should probably delete this folder.
COPY --chown=root:root bin /drupal/bin

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
